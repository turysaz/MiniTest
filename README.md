# MiniTest library

A minimal, single-source-file unit testing library for C#.
This is meant for projects so small that you are probably even using `csc` directly, without MsBuild or the like.

## Example

```csharp
using MiniTest;
public class MyTests {
    public static void Main(string[] args) {
        var suite = new MiniTestSuite();
        suite.Add(new TestCase("My Test 1", Test1));
        suite.Add(new TestCase("My Test 2", Test2));
        suite.Run();
    }

    private static void Test1() {
        MiniAssert.Equal(2, 2, "because that's equal");
    }

    private static async Task Test2() {
        var result = await MyApiAsync();
        MiniAssert.Contains(result, "foo", "because I say so.");
    }
}
```

## Build

**Option 1**: Don't build separately.
Just copy the file to your code and use it.

**Option 2**:
Create a project and build as DLL/Assembly.
Without MsBuild:

    # build
    csc -target:library -out:MiniTest.dll MiniTest.cs
    # use
    csc -r:MiniTest.dll MySourceFile.cs ...

