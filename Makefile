
CS=csc
CFLAGS=

all: MiniTest.dll

clean:
	rm *.dll

MiniTest.dll : MiniTest.cs
	$(CS) $(CFLAGS) -target:library -out:$@ $^

.PHONY : all
.PHONY : clean

