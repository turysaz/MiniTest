/*
MiniTest library
Copyright (c) 2022 Turysaz

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

namespace MiniTest {

    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    ///     The assertion exception.
    /// </summary>
    public class MiniTestException : Exception {
        public MiniTestException(string message)
            : base(message) { }
    }

    /// <summary>
    ///     Defines a single test case.
    /// </summary>
    public class TestCase{

        public TestCase(string name, Func<Task> testAction) {
            this.Name = name;
            this.Action = testAction;
        }

        public TestCase(string name, Action testAction) {
            this.Name = name;
            this.Action = () => {
                testAction();
                return Task.CompletedTask;
            };
        }

        /// <summary>
        ///     The test's name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        ///     The test code.
        /// </summary>
        public Func<Task> Action { get; }
    }

    /// <summary>
    ///     Defines static assertion methods.
    /// </summary>
    public static class MiniAssert {

        /// <summary>
        ///     Make the test fail immediately.
        /// </summary>
        public static void Fail(string because = "") =>
            throw new MiniTestException($"Test failed explicitly. {because}");

        /// <summary>
        ///     Asserts that <paramref name="actual"/> matches the
        ///     <paramref name="expected" /> value.
        /// </summary>
        public static void Equal<T>(T actual, T expected, string because = "") {
            if (expected == null) {
                if (actual != null) {
                    throw new MiniTestException(
                        $"Expected null {because}, but found {actual}.");
                }
                return;
            }
            if (!expected.Equals(actual)) {
                throw new MiniTestException($"{actual} should be {expected} {because}.");
            }
        }

        /// <summary>
        ///     Asserts that <paramref name="actual"/> does not match the
        ///     <paramref name="expected" /> value.
        /// </summary>
        public static void NotEqual<T>(T actual, T expected, string because = "") {
            if (actual == null) {
                if (expected == null) {
                    throw new MiniTestException(
                        $"Expected null {because}, but found {actual}.");
                }
                return;
            }
            if (actual.Equals(expected)) {
                throw new MiniTestException(
                    $"{actual} is same as {expected}, but excepted differently {because}");
            }
        }

        /// <summary>
        ///     Asserts that the <paramref name="actual"/> string contains
        ///     <paramref name="expected" /> as a sub string.
        /// </summary>
        public static void Contains(
                string actual,
                string expected,
                string because = "") {
            if (expected == null)
                throw new ArgumentNullException(nameof(expected));
            if (actual == null)
                throw new MiniTestException("String is null!");
            if (!actual.Contains(expected))
                throw new MiniTestException(
                    $"Expected \"{actual}\" to contain \"{expected}\""
                    + $" {because}.");
        }

        /// <summary>
        ///     Asserts that two arrays have equivalent content.
        /// </summary>
        public static void Equivalent<T>(
                T[] actual,
                T[] expected,
                string because = "") {

            if (expected == null || because == null) {
                throw new ArgumentNullException();
            }

            if (actual == null) {
                throw new MiniTestException("Array is null!");
            }

            if (actual.Length != expected.Length) {
                throw new MiniTestException(
                    $"Expected the arrays to be equivalent {because},"
                    + " but they differ in length."
                    + $" Expected length: {expected.Length}."
                    + $" Actual length: {actual.Length}");
            }

            for (var i = 0; i < actual.Length; i++) {
                var a = actual[i];
                var e = expected[i];
                if (e == null)
                {
                    if (a != null) {
                        throw new MiniTestException(
                            $"Expected value at index {i} to be null {because}"
                            + $", but was {a}.");
                    }

                    continue;
                }

                if (! e.Equals(a)) {
                    throw new MiniTestException(
                        $"Expected value at index {i} to be {e} {because}"
                        + $", but was {a}.");
                }
            }
        }
    }

    /// <summary>
    ///     Defines a test suite.
    /// </summary>
    public class MiniTestSuite {

        private List<TestCase> tests = new List<TestCase>();

        /// <summary>
        ///     Adds a test case to the test suite.
        /// </summary>
        public void Add(TestCase testCase) => this.tests.Add(testCase);

        /// <summary>
        ///     Runs the test suite.
        /// </summary>
        /// <remarks>
        ///     Tests run in random order.
        /// </remarks>
        public void Run() {
            Console.WriteLine("Starting tests...\n");

            // stats
            var successCount = 0;
            var failCount = 0;
            var crashCount = 0;

            // run tests in random order
            var rand = new Random();
            var shuffled = tests.OrderBy(_ => rand.Next()).ToList();

            // run tests
            var runningTests = shuffled.Select(t => RunTestAsync(t));
            Task.WhenAll(runningTests).Wait();

            Console.WriteLine(
                "\nTest run finished.\n"
                + $"Failed:    {failCount}\n"
                + $"Succeeded: {successCount}\n"
                + $"Crashed:   {crashCount}\n");

            Console.WriteLine(failCount + crashCount == 0 ? "SUCCESS" : "FAILURE");
            Console.WriteLine("=======");

            async Task RunTestAsync(TestCase testCase) {
                try {
                    await testCase.Action();
                    Interlocked.Increment(ref successCount);
                    Console.WriteLine($"Success: {testCase.Name}");
                } catch (MiniTestException aex) {
                    Console.WriteLine($"FAIL: {testCase.Name}: {aex.Message}");
                    Interlocked.Increment(ref failCount);
                } catch (Exception ex) {
                    Console.WriteLine($"EXCEPTION: {testCase.Name}: {ex}");
                    Interlocked.Increment(ref crashCount);
                }
            }
        }
    }
}

